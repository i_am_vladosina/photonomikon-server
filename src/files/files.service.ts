import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as fs from 'fs';
import * as path from 'path';
import * as uuid from 'uuid';

@Injectable()
export class FilesService {
  // TODO Валидация файла https://docs.nestjs.com/techniques/file-upload
  // TODO Сжатие и нарезка картинок
  public async createFile(file: Express.Multer.File): Promise<string> {
    try {
      const ext = file.originalname.split('.').pop();
      const name = `${uuid.v4()}.${ext}`;
      const filePath = path.resolve(__dirname, '..', 'static');

      // TODO Лучше асинхронная запись
      if (!fs.existsSync(filePath)) {
        fs.mkdirSync(filePath, { recursive: true });
      }

      fs.writeFileSync(path.join(filePath, name), file.buffer);

      return name;
    } catch (e) {
      throw new HttpException(
        'Ошибка при записи файла на диск',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}

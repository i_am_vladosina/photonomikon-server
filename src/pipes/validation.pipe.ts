import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';
import {
  ValidationException,
  ValidationRecords,
} from 'src/exceptions/validation.exception';

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  public async transform(value: any, metadata: ArgumentMetadata): Promise<any> {
    const obj = plainToClass(metadata.metatype, value);
    const errors = await validate(obj);

    if (errors.length) {
      const messages = errors.reduce<ValidationRecords>((errors, e) => {
        errors[e.property] = Object.values(e.constraints);

        return errors;
      }, {});

      throw new ValidationException(messages);
    }

    return value;
  }
}

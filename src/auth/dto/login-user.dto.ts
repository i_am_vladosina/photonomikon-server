import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsEmail } from 'class-validator';

export class LoginUserDto {
  @ApiProperty({
    example: 'name@domain.com',
    description: 'Адрес электронной почты',
  })
  @IsString({ message: 'email должен быть строкой' })
  @IsEmail({}, { message: 'Некорретный email' })
  public readonly email: string;

  @ApiProperty({
    example: 'qwerty',
    description: 'Пароль',
  })
  @IsString({ message: 'password должен быть строкой' })
  public readonly password: string;
}

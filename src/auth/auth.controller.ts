import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateUserDto } from 'src/Users/dto/create-user.dto';
import { AuthService } from './auth.service';
import { LoginUserDto } from './dto/login-user.dto';

@ApiTags('Авторизация')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/login')
  public login(@Body() loginDto: LoginUserDto) {
    return this.authService.login(loginDto);
  }

  @Post('/register')
  public register(@Body() userDto: CreateUserDto) {
    return this.authService.register(userDto);
  }
}

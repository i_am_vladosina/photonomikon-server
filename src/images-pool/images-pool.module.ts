import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { FilesModule } from 'src/files/files.module';
import { UserModel } from 'src/users/user.model';
import { ImagesPoolController } from './images-pool.controller';
import { ImagesPoolModel } from './images-pool.model';
import { ImagesPoolService } from './images-pool.service';

@Module({
  controllers: [ImagesPoolController],
  providers: [ImagesPoolService],
  imports: [
    SequelizeModule.forFeature([UserModel, ImagesPoolModel]),
    FilesModule,
  ],
})
export class ImagesPoolModule {}

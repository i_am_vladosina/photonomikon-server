import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { FilesService } from 'src/files/files.service';
import { CreateImageDto } from './dto/create-image.dto';
import { ImagesPoolModel } from './images-pool.model';

@Injectable()
export class ImagesPoolService {
  constructor(
    @InjectModel(ImagesPoolModel)
    private imagesPoolModel: typeof ImagesPoolModel,
    private filesService: FilesService,
  ) {}

  public async uploadImage(
    dto: CreateImageDto,
    imageFile: Express.Multer.File,
  ) {
    const fileName = await this.filesService.createFile(imageFile);
    const image = await this.imagesPoolModel.create({
      ...dto,
      image: fileName,
    });
    return image;
  }
}

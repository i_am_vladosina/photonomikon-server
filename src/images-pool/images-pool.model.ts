import { ApiProperty } from '@nestjs/swagger';
import {
  Model,
  Table,
  Column,
  DataType,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import { UserModel } from 'src/users/user.model';
import * as uuid from 'uuid';

interface ImageCreationAttrs {
  userId: number;
  image: string;
}

@Table({ tableName: 'images-pool' })
export class ImagesPoolModel extends Model<
  ImagesPoolModel,
  ImageCreationAttrs
> {
  @ApiProperty({
    example: '3153426523',
    description: 'Уникальный идентификатор',
  })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @ApiProperty({
    example: uuid.v4() + '.jpg',
    description: 'Название изображения',
  })
  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  image: string;

  @ForeignKey(() => UserModel)
  @Column({ type: DataType.INTEGER })
  userId: number;

  @BelongsTo(() => UserModel, {
    foreignKey: 'id',
  })
  author: UserModel;
}

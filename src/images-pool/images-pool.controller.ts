import {
  Body,
  Controller,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { CreateImageDto } from './dto/create-image.dto';
import { ImagesPoolService } from './images-pool.service';

@Controller('images-pool')
export class ImagesPoolController {
  constructor(private imagesPoolService: ImagesPoolService) {}

  @Post()
  @UseInterceptors(FileInterceptor('image'))
  public async uploadImage(
    @Body() dto: CreateImageDto,
    @UploadedFile() imageFile: Express.Multer.File,
  ) {
    return await this.imagesPoolService.uploadImage(dto, imageFile);
  }
}

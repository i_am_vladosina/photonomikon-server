import { HttpException, HttpStatus } from '@nestjs/common';

export type ValidationRecords = Record<string, string[]>;

export class ValidationException extends HttpException {
  private messages: ValidationRecords;

  constructor(response: ValidationRecords) {
    super(response, HttpStatus.BAD_REQUEST);
    this.messages = response;
  }
}

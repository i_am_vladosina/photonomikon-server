import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateRoleDto {
  @ApiProperty({
    example: 'ADMIN',
    description: 'Роль пользователя',
  })
  @IsString({
    message: 'role должнен быть строкой',
  })
  readonly role: string;

  @ApiProperty({
    example: 'Администратор',
    description: 'Описание роли пользователя',
  })
  @IsString({
    message: 'description должнен быть строкой',
  })
  readonly description: string;
}

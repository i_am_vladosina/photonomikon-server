import { ApiProperty } from '@nestjs/swagger';
import {
  Model,
  Table,
  Column,
  DataType,
  BelongsToMany,
  HasMany,
} from 'sequelize-typescript';
import { ImagesPoolModel } from 'src/images-pool/images-pool.model';
import { RoleModel } from 'src/roles/roles.model';
import { UserRolesModel } from 'src/roles/user-roles.model';

interface UserCreationAttrs {
  email: string;
  password: string;
}

@Table({ tableName: 'users' })
export class UserModel extends Model<UserModel, UserCreationAttrs> {
  @ApiProperty({
    example: '62114452314',
    description: 'Уникальный идентификатор',
  })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @ApiProperty({
    example: 'name@domain.com',
    description: 'Адрес электронной почты',
  })
  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  email: string;

  @ApiProperty({
    example: 'qwerty',
    description: 'Пароль',
  })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  password: string;

  @ApiProperty({
    example: '89005353535',
    description: 'Номер телефона мобильного формата',
  })
  @Column({
    type: DataType.STRING,
    allowNull: true,
  })
  phoneNumber: number;

  @ApiProperty({
    example: 'account',
    description: 'Никнейм/псевдоним',
  })
  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  accountName: string;

  @ApiProperty({
    example: 'Семен',
    description: 'Имя пользователя',
  })
  @Column({
    type: DataType.STRING,
    allowNull: true,
  })
  name: string;

  @ApiProperty({
    example: 'Свалов',
    description: 'Фамилия пользователя',
  })
  @Column({
    type: DataType.STRING,
    allowNull: true,
  })
  lastName: string;

  @BelongsToMany(() => RoleModel, {
    through: {
      model: () => UserRolesModel,
      unique: false,
    },
    foreignKey: 'userId',
    constraints: false,
  })
  roles: RoleModel[];

  // TODO + Roles
  // @Column({
  //   type: DataType.BOOLEAN,
  //   defaultValue: false,
  // })
  // isBanned: boolean;

  // @Column({
  //   type: DataType.STRING,
  //   allowNull: true,
  // })
  // banReason: string;

  @HasMany(() => ImagesPoolModel, 'id')
  images: ImagesPoolModel[];
}

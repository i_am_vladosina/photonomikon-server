import { Module } from '@nestjs/common';
import { forwardRef } from '@nestjs/common/utils/forward-ref.util';
import { SequelizeModule } from '@nestjs/sequelize';
import { AuthModule } from 'src/auth/auth.module';
import { ImagesPoolModel } from 'src/images-pool/images-pool.model';
import { RoleModel } from 'src/roles/roles.model';
import { RolesModule } from 'src/roles/roles.module';
import { UserRolesModel } from 'src/roles/user-roles.model';
import { UserModel } from './user.model';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

@Module({
  controllers: [UsersController],
  providers: [UsersService],
  imports: [
    SequelizeModule.forFeature([
      UserModel,
      RoleModel,
      UserRolesModel,
      ImagesPoolModel,
    ]),
    RolesModule,
    forwardRef(() => AuthModule),
  ],
  exports: [UsersService],
})
export class UsersModule {}

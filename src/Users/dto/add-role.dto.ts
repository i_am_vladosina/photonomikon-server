import { IsString, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class AddRoleDto {
  @ApiProperty({
    example: 'ADMIN',
    description: 'Роль пользователя',
  })
  @IsString({
    message: 'role должнен быть строкой',
  })
  public readonly role: string;

  @ApiProperty({
    example: 1,
    description: 'ID пользователя',
  })
  @IsNumber(
    {},
    {
      message: 'userId должно быть числом',
    },
  )
  public readonly userId: number;
}

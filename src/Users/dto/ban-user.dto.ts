import { IsString, IsNumber } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class BanUserDto {
  @ApiProperty({
    example: 1,
    description: 'ID пользователя',
  })
  @IsNumber({}, { message: 'userId должен быть строкой' })
  public readonly userId: number;
  @ApiProperty({
    example: 'Хулиганство',
    description: 'Причина блокировки',
  })
  @IsString({ message: 'banReason должен быть строкой' })
  public readonly banReason: string;
}

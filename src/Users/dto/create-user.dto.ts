import { ApiProperty } from '@nestjs/swagger';
import { IsString, Length, IsEmail } from 'class-validator';

export class CreateUserDto {
  @ApiProperty({
    example: 'name@domain.com',
    description: 'Адрес электронной почты',
  })
  @IsString({ message: 'email должен быть строкой' })
  @IsEmail({}, { message: 'Некорретный email' })
  public readonly email: string;

  @ApiProperty({
    example: 'qwerty',
    description: 'Пароль',
  })
  @IsString({ message: 'password должен быть строкой' })
  @Length(8, 128, { message: 'Пароль должен быть не менее 8 символов' })
  public readonly password: string;

  @ApiProperty({
    example: 'account',
    description: 'Никнейм/псевдоним',
  })
  @IsString({ message: 'accountName должен быть строкой' })
  public readonly accountName: string;
}

import { Injectable, HttpException } from '@nestjs/common';
import { HttpStatus } from '@nestjs/common/enums';
import { InjectModel } from '@nestjs/sequelize';
import { RolesService } from 'src/roles/roles.service';
import { AddRoleDto } from './dto/add-role.dto';
import { BanUserDto } from './dto/ban-user.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { UserModel } from './user.model';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(UserModel) private readonly userModel: typeof UserModel,
    private readonly roleService: RolesService,
  ) {}

  public async createUser(dto: CreateUserDto): Promise<UserModel> {
    const user = await this.userModel.create(dto);
    const role = await this.roleService.getRole('USER');
    await user.$set('roles', [role.id]);
    user.roles = [role];
    return user;
  }

  public async getAllUsers(): Promise<UserModel[]> {
    return await this.userModel.findAll({ include: { all: true } });
  }

  public async getUserByEmail(email: string) {
    const user = await this.userModel.findOne({
      where: { email },
    });

    return user;
  }

  public async addRole(addRoleDto: AddRoleDto) {
    const user = await this.userModel.findByPk(addRoleDto.userId);
    const role = await this.roleService.getRole(addRoleDto.role);

    // TODO Нужно проверять, чтобы у юзера не дублировались роли
    if (user && role) {
      await user.$add('role', role.id);
      return addRoleDto;
    }

    throw new HttpException(
      'Пользователь или роль не найдена',
      HttpStatus.NOT_FOUND,
    );
  }

  // TODO
  // public async banUser(banUserDto: BanUserDto) {
  //   return;
  // }
}

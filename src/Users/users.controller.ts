import { Controller, Post, Get, Body, UseGuards } from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';
import { ApiTags } from '@nestjs/swagger/dist';
import { Roles } from 'src/auth/roles-auth.decorator';
import { RolesGuard } from 'src/auth/roles.guard';
import { AddRoleDto } from './dto/add-role.dto';
import { BanUserDto } from './dto/ban-user.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { UserModel } from './user.model';
import { UsersService } from './users.service';

@ApiTags('Пользователи')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @ApiOperation({
    summary: 'Создание юзера',
  })
  @ApiResponse({ status: 200, type: UserModel })
  @Post()
  public create(@Body() userDto: CreateUserDto): Promise<UserModel> {
    return this.usersService.createUser(userDto);
  }

  @ApiOperation({
    summary: 'Получение списка юзеров',
  })
  @ApiResponse({ status: 200, type: [UserModel] })
  @Roles('ADMIN')
  @UseGuards(RolesGuard)
  @Get()
  public getAllUsers(): Promise<UserModel[]> {
    return this.usersService.getAllUsers();
  }

  @ApiOperation({
    summary: 'Выдача роли',
  })
  @ApiResponse({ status: 200 })
  @Roles('ADMIN')
  @UseGuards(RolesGuard)
  @Post('/role')
  public addRole(@Body() addRoleDto: AddRoleDto): Promise<AddRoleDto> {
    return this.usersService.addRole(addRoleDto);
  }

  // TODO
  // @ApiOperation({
  //   summary: 'Бан пользователя',
  // })
  // @ApiResponse({ status: 200 })
  // @Roles('ADMIN')
  // @UseGuards(RolesGuard)
  // @Post('/ban')
  // public banUser(@Body() banUserDto: BanUserDto) {
  //   //return this.usersService.addRole(addRoleDto);
  // }
}
